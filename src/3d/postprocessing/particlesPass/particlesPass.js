var Effect = require('../Effect')
var particles = require('../../particles')

var exports = module.exports = new Effect()
var _super = Effect.prototype

exports.init = init
exports.render = render

function init () {
  _super.init.call(this)
}

function render (dt, renderTarget, toScreen) {
  particles.update(renderTarget, dt)
}
