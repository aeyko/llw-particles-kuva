var effectComposer = require('./effectComposer')
var fxaa = require('./fxaa/fxaa')
var bloom = require('./bloom/bloom')
var vignette = require('./vignette/vignette')
var motionBlur = require('./motionBlur/motionBlur')
var dof = require('./dof/dof')
var particlesPass = require('./particlesPass/particlesPass')
var fboHelper = require('../fboHelper')

var undef

exports.init = init
exports.resize = resize
exports.render = render
exports.visualizeTarget = undef

function init (renderer, scene, camera) {
  effectComposer.init(renderer, scene, camera)

  // fxaa.init(true);

  particlesPass.init()
  effectComposer.queue.push(particlesPass)

  /* dof.init()
  effectComposer.queue.push(dof) */

  motionBlur.init()
  effectComposer.queue.push(motionBlur)

  /* bloom.init()
  effectComposer.queue.push(bloom) */

  fxaa.init()
  effectComposer.queue.push(fxaa)

  vignette.init()
  effectComposer.queue.push(vignette)
}

function resize (width, height) {
  effectComposer.resize(width, height)
}

function render (dt) {
  effectComposer.renderQueue(dt)

  if (exports.visualizeTarget) {
    fboHelper.copy(exports.visualizeTarget.texture)
  }
}
