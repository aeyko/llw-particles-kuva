const settings = require('../core/settings')
const THREE = require('three')
const mathUtils = require('../utils/math')

var undef

const glslify = require('glslify')

var _copyShader
var _positionShader
var _textureDefaultPosition
var _positionRenderTarget
var _positionRenderTarget2

var _renderer
var _mesh
var _scene
var _camera

var TEXTURE_WIDTH
var TEXTURE_HEIGHT
var AMOUNT

var EXTERN_SET_X = 0
var EXTERN_SET_Y = 0

var USE_VIDEO_DATA = false;

exports.init = init
exports.update = update
exports.positionRenderTarget = undef
exports.prevPositionRenderTarget = undef
exports.initAnimation = 0
exports.updatePosition = externUpdatePosition
exports.useVideoPositionData = useVideoPositionData

exports.videoFrame = 0

function init (renderer) {
  TEXTURE_WIDTH = exports.TEXTURE_WIDTH = settings.simulatorTextureWidth
  TEXTURE_HEIGHT = exports.TEXTURE_HEIGHT = settings.simulatorTextureHeight
  AMOUNT = exports.AMOUNT = TEXTURE_WIDTH * TEXTURE_HEIGHT

  _renderer = renderer

  var rawShaderPrefix = 'precision ' + renderer.capabilities.precision + ' float;\n'

  var gl = _renderer.getContext()
  if (!gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS)) {
    alert('No support for vertex shader textures!')
    return
  }
  if (!gl.getExtension('OES_texture_float')) {
    alert('No OES_texture_float support for float textures!')
    return
  }

  _scene = new THREE.Scene()
  _camera = new THREE.Camera()
  _camera.position.z = 1

  _copyShader = new THREE.RawShaderMaterial({
    uniforms: {
      resolution: { type: 'v2', value: new THREE.Vector2(TEXTURE_WIDTH, TEXTURE_HEIGHT) },
      texture: { type: 't', value: undef }
    },
    vertexShader: rawShaderPrefix + glslify('../glsl/quad.vert'),
    fragmentShader: rawShaderPrefix + glslify('../glsl/through.frag')
  })

  _positionShader = new THREE.RawShaderMaterial({
    uniforms: {
      resolution: { type: 'v2', value: new THREE.Vector2(TEXTURE_WIDTH, TEXTURE_HEIGHT) },
      texturePosition: { type: 't', value: undef },
      textureDefaultPosition: { type: 't', value: undef },
      videoXY: { type: 'v2', value: new THREE.Vector3() },
      mouse3d: { type: 'v3', value: new THREE.Vector3() },
      speed: { type: 'f', value: 0 },
      dieSpeed: { type: 'f', value: 0 },
      deltaDistance: { type: 'f', value: 0 },
      radius: { type: 'f', value: 0 },
      attraction: { type: 'f', value: 0 },
      time: { type: 'f', value: 0 },
      initAnimation: { type: 'f', value: 0 },
      curlSize: { type: 'f', value: 0.015 }
    },
    vertexShader: rawShaderPrefix + glslify('../glsl/quad.vert'),
    fragmentShader: rawShaderPrefix + glslify('../glsl/position.frag'),
    blending: THREE.NoBlending,
    transparent: false,
    depthWrite: false,
    depthTest: false
  })

  _mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(2, 2), _copyShader)
  _scene.add(_mesh)

  _positionRenderTarget = new THREE.WebGLRenderTarget(TEXTURE_WIDTH, TEXTURE_HEIGHT, {
    wrapS: THREE.ClampToEdgeWrapping,
    wrapT: THREE.ClampToEdgeWrapping,
    minFilter: THREE.NearestFilter,
    magFilter: THREE.NearestFilter,
    format: THREE.RGBAFormat,
    type: settings.floatType,
    depthWrite: false,
    depthBuffer: false,
    stencilBuffer: false
  })
  _positionRenderTarget2 = _positionRenderTarget.clone()
  _copyTexture(_createPositionTexture(), _positionRenderTarget)
  _copyTexture(_positionRenderTarget, _positionRenderTarget2)
}

function _copyTexture (input, output) {
  _mesh.material = _copyShader
  _copyShader.uniforms.texture.value = input.texture
  _renderer.render(_scene, _camera, output)
}

function _updatePosition (dt) {
  // swap
  var tmp = _positionRenderTarget
  _positionRenderTarget = _positionRenderTarget2
  _positionRenderTarget2 = tmp

  _mesh.material = _positionShader
  _positionShader.uniforms.textureDefaultPosition.value = _textureDefaultPosition
  _positionShader.uniforms.texturePosition.value = _positionRenderTarget2.texture
  _positionShader.uniforms.deltaDistance.value = settings.deltaDistance
  _positionShader.uniforms.time.value += dt * 0.001
  _renderer.render(_scene, _camera, _positionRenderTarget)
}

function externUpdatePosition(x, y) {
  EXTERN_SET_X = x
  EXTERN_SET_Y = y
}

function _createPositionTexture () {
  var positions = new Float32Array(AMOUNT * 4)
  var i4
  var r, phi, theta
  for (var i = 0; i < AMOUNT; i++) {
    i4 = i * 4
    r = (0.5 + Math.random() * 0.5) * 75
    phi = (Math.random() - 0.5) * Math.PI
    theta = Math.random() * Math.PI * 2
    positions[i4 + 0] = r * Math.cos(theta) * Math.cos(phi)
    positions[i4 + 1] = r * Math.sin(phi)
    positions[i4 + 2] = r * Math.sin(theta) * Math.cos(phi)
    positions[i4 + 3] = Math.random()
  }
  var texture = new THREE.DataTexture(positions, TEXTURE_WIDTH, TEXTURE_HEIGHT, THREE.RGBAFormat, THREE.FloatType)
  texture.minFilter = THREE.NearestFilter
  texture.magFilter = THREE.NearestFilter
  texture.needsUpdate = true
  texture.generateMipmaps = false
  texture.flipY = false
  _textureDefaultPosition = texture
  return texture
}

var videoFrameNext = 0
var videoFramePrev = 0
var videoFrameRate = settings.videoFrameRate
var videoPos = new THREE.Vector2()

function update (dt) {
  var deltaRatio = dt / 16.6667

  dt = dt * settings.speed

  if (settings.speed || settings.dieSpeed) {
    var autoClearColor = _renderer.autoClearColor
    var clearColor = _renderer.getClearColor().getHex()
    var clearAlpha = _renderer.getClearAlpha()

    _renderer.autoClearColor = false

    _positionShader.uniforms.curlSize.value = settings.curlSize
    _positionShader.uniforms.dieSpeed.value = settings.dieSpeed * deltaRatio
    _positionShader.uniforms.radius.value = settings.radius
    _positionShader.uniforms.attraction.value = settings.attraction * settings.speed * deltaRatio
    _positionShader.uniforms.speed.value = settings.speed * deltaRatio
    _positionShader.uniforms.initAnimation.value = exports.initAnimation

    videoFrameNext = Math.ceil(settings.videoEl.currentTime * videoFrameRate)
    videoFramePrev = Math.floor(settings.videoEl.currentTime * videoFrameRate)

    //OLD METHOD OF GETTING POSITION FROM VIDEO DATA
    var keyFrameData = settings.videoPositionJSONData.data.source.property.keyframe

    if (
      typeof keyFrameData[videoFramePrev] !== 'undefined' &&
      typeof keyFrameData[videoFrameNext] !== 'undefined' && USE_VIDEO_DATA
    ) {
      var videoFramePrevValX = parseFloat(keyFrameData[videoFramePrev]._value0)
      var videoFramePrevValY = parseFloat(keyFrameData[videoFramePrev]._value1)
      var videoFrameNextValX = parseFloat(keyFrameData[videoFrameNext]._value0)
      var videoFrameNextValY = parseFloat(keyFrameData[videoFrameNext]._value1)

      var xVal = videoFramePrevValX + deltaRatio * (videoFrameNextValX - videoFramePrevValX)
      var yVal = videoFramePrevValY + deltaRatio * (videoFrameNextValY - videoFramePrevValY)

      // x, inMin, inMax, outMin, outMax
      videoPos.x = mathUtils.map(
        xVal,
        0,
        settings.videoWidth,
        -400,
        400
      )

      videoPos.y = mathUtils.map(
        yVal,
        0,
        settings.videoHeight,
        -400,
        400
      )


      _positionShader.uniforms.videoXY.value.copy(videoPos)
    } else {
      //NEW METHOD OF JUST MANUALLY SETTING IT
      videoPos.x = EXTERN_SET_X
      videoPos.y = EXTERN_SET_Y
    }



    _positionShader.uniforms.videoXY.value.copy(videoPos)

    _positionShader.uniforms.mouse3d.value.copy(settings.mouse3d)
    _updatePosition(dt)

    _renderer.setClearColor(clearColor, clearAlpha)
    _renderer.autoClearColor = autoClearColor
    exports.positionRenderTarget = _positionRenderTarget
    exports.prevPositionRenderTarget = _positionRenderTarget2
  }
}

function useVideoPositionData(useData) {
  USE_VIDEO_DATA = useData;
}
