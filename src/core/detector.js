var undef

var _prefixes = 'Webkit Moz O ms'.split(' ')
var _dummyStyle = document.createElement('div').style
var _win = window
var _doc = document
var _ua = (navigator.userAgent || navigator.vendor || window.opera).toLowerCase()

var exports = module.exports = {

  videoFormat: _testMediaFormat('video', ['video/mp4']),
  audioFormat: _testMediaFormat('audio', ['audio/mp3']),

  isIFrame: _win.self !== _win.top,

  isRetina: _win.devicePixelRatio && _win.devicePixelRatio >= 1.5,
  isSupportOpacity: _dummyStyle.opacity !== undef,

  isChrome: _ua.indexOf('chrome') > -1,
  isFirefox: _ua.indexOf('firefox') > -1,
  isSafari: _ua.indexOf('safari') > -1,
  isEdge: _ua.indexOf('edge') > -1,
  isIE: _ua.indexOf('msie') > -1,

  isMobile: /(iPad|iPhone|Android)/i.test(_ua),
  isIOS: /(iPad|iPhone)/i.test(_ua),

  filterStyle: _getStyleName('filter'),
  transitionStyle: _getStyleName('transition'),
  transformStyle: _getStyleName('transform'),
  transform3dStyle: _getStyleName('transform', 'perspective'),
  transformPerspectiveStyle: _getStyleName('perspective'),
  transformOriginStyle: _getStyleName('transformOrigin')

}

if (exports.isIE) {
  exports.filterStyle = undef
}

if (exports.isEdge) {
  exports.isChrome = exports.isSafari = false
}

// helpers

function _testMediaFormat (type, orders) {
  var dom
  try {
    switch (type) {
      case 'video':
        dom = new _win.Video()
        break
      case 'audio':
        dom = new _win.Audio()
        break
      default:
                // add error
    }
  } catch (e) {
    dom = _doc.createElement(type)
  }
  var format
  for (var i = 0, len = orders.length; i < len; i++) {
    if (dom.canPlayType && dom.canPlayType(orders[i])) {
      format = orders[i].substr(orders[i].indexOf('/') + 1)
      break
    }
  }
  return format
}

function _getStyleName (prop, refProp) {
  if (!refProp) refProp = prop

  return _getPropFromIndex(prop, _getPropIndex(refProp))
}

function _getPropIndex (prop) {
  var ucProp = prop.charAt(0).toUpperCase() + prop.slice(1)
  var i = _prefixes.length
  while (i--) {
    if (_dummyStyle[_prefixes[i] + ucProp] !== undef) {
      return i + 2
    }
  }
  if (_dummyStyle[prop] !== undef) {
    return 1
  }
  return 0
}

function _getPropFromIndex (prop, index) {
  return index > 1 ? _prefixes[index - 2] + prop.charAt(0).toUpperCase() + prop.slice(1) : index == 1 ? prop : false
}
