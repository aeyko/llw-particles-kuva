
uniform sampler2D uTexturePosition;
varying vec2 vUv;

varying vec4 positionInfo;
varying float vLife;
varying vec4 mvPosition;
varying vec4 vPos;

void main() {

    positionInfo = texture2D( uTexturePosition, position.xy );
    vec4 mvPosition = modelViewMatrix * vec4( positionInfo.xyz, 1.0 );

    vUv = uv;
    vPos = modelViewMatrix * vec4( position, 1.0 );
    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

}
