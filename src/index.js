(function () {
  const quickLoader = require('quick-loader')
  const raf = require('raf')

  const THREE = require('three')

  const settings = require('./core/settings')

  const math = require('./utils/math')

  const fboHelper = require('./3d/fboHelper')
  const simulator = require('./3d/simulator')
  const particles = require('./3d/particles')

  const postprocessing = require('./3d/postprocessing/postprocessing')
  const dof = require('./3d/postprocessing/dof/dof')
  const vignette = require('./3d/postprocessing/vignette/vignette')
  const motionBlur = require('./3d/postprocessing/motionBlur/motionBlur')
  const fxaa = require('./3d/postprocessing/fxaa/fxaa')
  const bloom = require('./3d/postprocessing/bloom/bloom')

  const StartAudioContext = require('startaudiocontext')

  var _width = 0
  var _height = 0

  var _camera
  var _scene
  var _renderer

  var _bgColor

  var _time = 0
  var _ray = new THREE.Ray()

  var _initAnimation = 0
  var _isSkipRendering = false

  var _audioContext
  var _audioSource = null

  var _baseCameraZ = 200.0
  var _targetCameraPos = new THREE.Vector3()

  var _cameraFollowMouse = true
  var _cameraLerpSpeed = 0.01

  var _targetDieSpeed = 0.009
  var _maxDieSpeed = 0.1
  var _dieSpeedDirection = 'expand' // expand or contract

  var _videoTimeout

  const JSONLoader = new THREE.FileLoader()

  // HV CHANGE video -> kuvaVideo
  const videoEl = document.getElementById('kuvaVideo')
  const videoPlayBut = document.getElementById('video-play')
  const videoPauseBut = document.getElementById('video-pause')

  function loadVideoPositionJSON (path) {
    return new Promise(function (resolve, reject) {
      JSONLoader.load(
      // HV CHANGE get path from data-attribute on the video tag
      videoEl.dataset.json,
      function (data) {
        try {
          settings.videoPositionJSONData = JSON.parse(data)
          resolve()
        } catch (error) {
          reject(error)
        }
      },
      function (xhr) {
        console.log((xhr.loaded / xhr.total * 100) + '% loaded')
      },
      function (error) {
        reject(error)
      }
    )
    })
  }

  function startVisualiser () {
    videoEl.play()
    bindAudioAnalyzer()
    startMotion()
  }

  function stopVisualiser () {
    videoEl.pause()
    stopMotion()
  }

  function bindVideoButtons () {
    settings.videoEl = videoEl
    settings.videoPlayBut = videoPlayBut
    settings.videoPauseBut = videoPauseBut

    if (videoPlayBut) {
      videoPlayBut.addEventListener('click', function (e) {
        e.preventDefault()
        startVisualiser()
      })
    }

    if (videoPauseBut) {
      videoPauseBut.addEventListener('click', function (e) {
        e.preventDefault()
        stopVisualiser()
      })
    }
  }

  var showVisualiser = function () {
    settings.dieSpeed = parseFloat(JSON.stringify(_maxDieSpeed))
    _dieSpeedDirection = 'expand'
    _cameraLerpSpeed = 0.01
    _cameraFollowMouse = true
    clearTimeout(_videoTimeout)
    settings.videoEl.classList.remove('hidden')
  }

  var showVideo = function () {
    _videoTimeout = setTimeout(function () {
      _dieSpeedDirection = 'contract'
      resetCameraPos()
      settings.videoEl.classList.add('hidden')
    }, 100)
  }

  function resetCameraPos () {
    _cameraFollowMouse = false
    _cameraLerpSpeed = 0.1
    _targetCameraPos.x = 0.0
    _targetCameraPos.y = 0.0
    _targetCameraPos.z = _baseCameraZ
  }

  function bindAudioAnalyzer () {
    if (!settings.analyzeAudio) {
      return
    }
    if (_audioSource === null) {
      _audioSource = _audioContext.createMediaElementSource(videoEl)
      _audioSource.connect(settings.analyser)
      settings.analyser.connect(_audioContext.destination)
    }
  }

  function initAudioContext () {
    window.AudioContext = window.AudioContext || window.webkitAudioContext
    if (window.AudioContext) {
      _audioContext = new window.AudioContext()
    }

    StartAudioContext(_audioContext, '#body', function () {

    })
  }

  function initAudioAnalyzer () {
    if (!settings.analyzeAudio) {
      return
    }
    settings.analyser = _audioContext.createAnalyser()
    settings.analyser.fftSize = 32
    settings.analyser.smoothingTimeConstant = 0.95
    settings.frequencyData = new Uint8Array(settings.analyser.frequencyBinCount)
  }

  function init () {
    initAudioContext()
    initAudioAnalyzer()

    _bgColor = new THREE.Color(settings.bgColor)
    settings.mouseX = 0
    settings.mouseY = 0
    settings.prevMouseX = 0
    settings.prevMouseY = 0
    settings.mouse = new THREE.Vector2(0, 0)
    settings.mouse3d = _ray.origin

    bindVideoButtons()

    _renderer = new THREE.WebGLRenderer({
      preserveDrawingBuffer: true
    })
    fboHelper.init(_renderer)

    document.body.appendChild(_renderer.domElement)

    _scene = new THREE.Scene()
    // _scene.fog = new THREE.Fog(_bgColor, 1)
    _camera = settings.camera = new THREE.PerspectiveCamera(60, 1, 20, 500000)

    _camera.position.set(0, 0, _baseCameraZ)
    settings.cameraPosition = _camera.position

    // gpu sniffing for weird blue colour bug
    var gl = _renderer.getContext()
    var gldebugInfo = gl.getExtension('WEBGL_debug_renderer_info')
    var glrenderer = gl.getParameter(gldebugInfo.UNMASKED_RENDERER_WEBGL).toLowerCase()
    var userAgent = (navigator.userAgent || navigator.vendor || window.opera).toLowerCase()

    var blueShift = glrenderer.indexOf('intel(r) hd graphics 6000') > -1 && userAgent.indexOf('macintosh; intel mac') > -1 && userAgent.indexOf('chrome') > -1

    // add quad as bg
    if (!blueShift) {
      var bgPlaneColour = new THREE.Color(0.0, 0.0, 0.1, 1.0)
      var bgGeometry = new THREE.PlaneBufferGeometry(50000, 50000, 1, 1)
      var bgMaterial = new THREE.MeshBasicMaterial({
        color: bgPlaneColour
      })
      var mesh = new THREE.Mesh(bgGeometry, bgMaterial)
      _scene.add(mesh)
    }

    // store particle positions
    simulator.init(_renderer)

    particles.init(_renderer, _camera)

    postprocessing.init(_renderer, _scene, _camera)

    motionBlur.maxDistance = 400
    motionBlur.motionMultiplier = 2
    motionBlur.linesRenderTargetScale = settings.motionBlurQualityMap[settings.query.motionBlurQuality]

    window.addEventListener('resize', _onResize)
    window.addEventListener('mousemove', _onMove)
    window.addEventListener('touchmove', _bindTouch(_onMove))
    window.addEventListener('keyup', _onKeyUp)

    settings.deltaDistance = 1
    settings.prevMouse = new THREE.Vector2(0, 0)
    _time = Date.now()
    _onResize()
    _loop()

    startVisualiser()
  }

  function stopMotion () {
    settings.speed = 0
    settings.dieSpeed = 0
  }

  function startMotion () {
    settings.speed = 0.25
    settings.dieSpeed = parseFloat(JSON.stringify(_targetDieSpeed))
  }

  function _bindTouch (func) {
    return function (evt) {
      func(evt.changedTouches[0])
    }
  }

  function _onMove (evt) {
    settings.mouseX = evt.clientX
    settings.mouseY = evt.clientY
    settings.mouse.x = (settings.mouseX / _width) * 2 - 1
    settings.mouse.y = -(settings.mouseY / _height) * 2 + 1
  }

  function _onKeyUp (evt) {
    if (evt.keyCode === 32) {
      _toggleMovement()
    }
  }

  function _toggleMovement () {
    settings.speed = settings.speed === 0 ? 0.25 : 0
    settings.dieSpeed = settings.dieSpeed === 0 ? parseFloat(JSON.stringify(_targetDieSpeed)) : 0
  }

  function _onResize () {
    _width = window.innerWidth
    _height = window.innerHeight

    particles.resize(_width, _height)
    postprocessing.resize(_width, _height)

    _camera.aspect = _width / _height
    _camera.updateProjectionMatrix()
    _renderer.setSize(_width, _height)
  }

  function _loop () {
    var newTime = Date.now()
    raf(_loop)
    if (!_isSkipRendering) _render(newTime - _time, newTime)
    _time = newTime
  }

  function _render (dt, newTime) {
    motionBlur.skipMatrixUpdate = true

    _bgColor.setStyle(settings.bgColor)

    _initAnimation = Math.min(_initAnimation + dt * 0.00025, 1)
    simulator.initAnimation = _initAnimation

    // update mouse3d
    _camera.updateMatrixWorld()
    _ray.origin.setFromMatrixPosition(_camera.matrixWorld)
    _ray.direction.set(settings.mouse.x, settings.mouse.y, 0.5).unproject(_camera).sub(_ray.origin).normalize()
    var distance = _ray.origin.length() / Math.cos(Math.PI - _ray.direction.angleTo(_ray.origin))
    _ray.origin.add(_ray.direction.multiplyScalar(distance * 1.0))

    settings.deltaDistance = math.distanceTo(settings.mouseX - settings.prevMouseX, settings.mouseY - settings.prevMouseY)
    if (settings.deltaDistance) {
      settings.deltaDistance /= 10
    }
    settings.prevMouse.copy(settings.mouse)

    settings.insetExtra += ((settings.speed ? 0 : 0.25) - settings.insetExtra) * dt * (settings.speed ? 0.01 : 0.003)

    simulator.update(dt)
    particles.preRender(dt)

    fxaa.enabled = !!settings.fxaa
    dof.enabled = !!settings.dof
    motionBlur.enabled = !!settings.motionBlur
    vignette.enabled = !!settings.vignette
    bloom.enabled = !!settings.bloom

    var uniforms = vignette.uniforms
    var reduction = (1.5 + settings.brightness * 1.5) * settings.vignetteMultiplier
    var boost = 0.8 + settings.brightness * 0.55
    uniforms.u_reduction.value += (reduction - uniforms.u_reduction.value) * 0.05
    uniforms.u_boost.value += (boost - uniforms.u_boost.value) * 0.05
    postprocessing.render(dt, newTime)

    settings.prevMouseX = settings.mouseX
    settings.prevMouseY = settings.mouseY

    var newPos = new THREE.Vector3(Math.sin(settings.mouse.x), Math.tan(settings.mouse.y), 0.0).multiplyScalar(100)
    var zPos = _baseCameraZ + ((newPos.length() * 0.1) * -1.0)

    if (_cameraFollowMouse) {
      _targetCameraPos.x = newPos.x
      _targetCameraPos.y = newPos.y
      _targetCameraPos.z = zPos
    }

    smoothCamera()

    if (_dieSpeedDirection === 'expand') {
      if (settings.dieSpeed > _targetDieSpeed) {
        settings.dieSpeed -= 0.001
      }
      if (settings.dieSpeed < _targetDieSpeed) {
        settings.dieSpeed = parseFloat(JSON.stringify(_targetDieSpeed))
      }
    } else {
      if (settings.dieSpeed < _maxDieSpeed) {
        settings.dieSpeed += 0.001
      }
      if (settings.dieSpeed > _maxDieSpeed) {
        settings.dieSpeed = parseFloat(JSON.stringify(_maxDieSpeed))
      }
    }
  }

  function smoothCamera () {
    var _camPosX = _camera.position.x + _cameraLerpSpeed * (_targetCameraPos.x - _camera.position.x)
    var _camPosY = _camera.position.y + _cameraLerpSpeed * (_targetCameraPos.y - _camera.position.y)
    var _camPosZ = _camera.position.z + _cameraLerpSpeed * (_targetCameraPos.z - _camera.position.z)

    _camera.position.x = _camPosX
    _camera.position.y = _camPosY
    _camera.position.z = _camPosZ

    _camera.lookAt(new THREE.Vector3(0.0, 0.0, 0.0))
  }

  // HV CHANGE get path from data-attribute on the video tag
  quickLoader.add(videoEl.dataset.texmetal, {
    onLoad: function (img) {
      settings.sphereMap = img
    }
  })

// public API methods
  window.particleVisualiser = {
    showVisualiser: showVisualiser,
    showVideo: showVideo,
    loadVideoPositionJSON: loadVideoPositionJSON,
    quickLoader: quickLoader,
    init: init,
    useVideoPositionData: simulator.useVideoPositionData,
    updatePosition: simulator.updatePosition,
  }
})()
